﻿using System;

namespace ExercitiiVectori
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] studenti = new string[3];

            studenti[0] = "Ana";
            studenti[1] = "Ion";
            studenti[2] = "Florica";
            // studenti[3] = "Ghita"; eroare

            // dimensiune
            Console.WriteLine("vectorul are " + studenti.Length + " elemente");

            // alternative pentru declararea vectorilor
            string[] s1 = new string[] {"Ion", "Maria", "Ghita"};
            string[] s2 = {"Ion", "Maria", "Florica"};

            // parcurgere
            // puneti <= si vedeti eroarea
            for (int i = 0; i < studenti.Length; i++)
            {
                Console.WriteLine("Studentul {0}: {1}", i + 1, studenti[i]);
            }

            // redimensionam vectorul si afisam din nou
            Array.Resize(ref studenti, 4);
            studenti[3] = "Ghita";

            for (int i = 0; i < studenti.Length; i++)
            {
                Console.WriteLine("Studentul {0}: {1}", i + 1, studenti[i]);
            }
            
            // preluam de la utilizator 5 numere intr-un vector si aflam suma si media lor
            
            // 2 vectori diferiti
            // preluam de la utilizator numele a 3 studenti
            // preluam de la utilizator varsta pentru cei 3 studenti
            // afisam Studentul X are 20 de ani
            

            // exercitii vectori, cele de pe github
            int[] numere =
            {
                135, 186, 60, 28, 121, 142, 176, 167, 166, 247, 85, 164, 132, 181, 180, 149, 50, 298, 289, 192, 90, 217,
                284, 72, 190, 122, 89, 183, 225, 223, 68, 250, 188, 126, 55, 292, 139, 155, 204, 273, 287, 153, 150,
                123, 26, 240, 39, 9, 12, 229, 131, 100, 111, 23, 178, 66, 165, 99, 120, 156, 171, 259, 11, 74, 232, 297,
                159, 84, 293, 234, 254, 19, 173, 184, 221, 158, 32, 212, 7, 210, 279, 275, 114, 77, 95, 270, 53, 281,
                21, 278, 109, 1, 63, 191, 174, 218, 58, 276, 75, 41, 47, 216, 236, 103, 285, 20, 38, 300, 71, 277, 88,
                86, 148, 13, 262, 203, 231, 6, 107, 228, 290, 235, 33, 5, 59, 127, 208, 206, 170, 108, 69, 286, 169,
                269, 185, 233, 267, 245, 263, 162, 97, 294, 288, 200, 8, 195, 62, 35, 27, 246, 65, 22, 264, 168, 226,
                25, 243, 161, 187, 40, 44, 198, 48, 136, 222, 157, 251, 272, 137, 105, 242, 43, 296, 29, 76, 94, 113,
                52, 73, 2, 79, 138, 244, 205, 92, 64, 199, 133, 3, 291, 104, 102, 249, 146, 193, 36, 24, 257, 91, 81
            };

            int suma = 0;
            double sumaNumerePare = 0;
            int contorNumerePare = 0;
            for (int i = 0; i < numere.Length; i++)
            {
                suma += numere[i];

                if (numere[i] % 2 == 0)
                {
                    sumaNumerePare += numere[i];
                    contorNumerePare++;
                }
            }

            Console.WriteLine("Suma numerelor este: {0}", suma); // 29971
            Console.WriteLine("Media numerelor pare este: {0}", sumaNumerePare / contorNumerePare); // 154.323232323232

            // preluam de la utilizator un numar si verificam daca exista in vector
            Console.WriteLine("Va rugam introduceti un numar intre 1 si 300");

            int.TryParse(Console.ReadLine(), out int numarUtil);
            bool existaNr = false;

            for (int i = 0; i < numere.Length; i++)
            {
                if (numere[i] == numarUtil)
                {
                    existaNr = true;
                    break; // dupa ce am gasit numarul nu are rost sa parcurgem restul vectorului
                }
            }

            if (existaNr)
                Console.WriteLine("Numarul intordus exista in vector");
            else
                Console.WriteLine("Numarul intordus NU exista in vector");

            // cate numere mai mici decat numarul intordus sunt in vector?
            int contorNumereMaiMici = 0;
            for (int i = 0; i < numere.Length; i++)
            {
                if (numere[i] < numarUtil)
                {
                    contorNumereMaiMici++;
                }
            }

            Console.WriteLine("Sunt {0} numere mai mici decat {1} in vectorul de mai sus.", contorNumereMaiMici,
                numarUtil);

            // cel mai mic numar din vector
            int nrMic = int.MaxValue;

            for (int i = 0; i < numere.Length; i++)
            {
                if (numere[i] < nrMic)
                {
                    nrMic = numere[i];
                }
            }

            Console.WriteLine("Cel mai mic numar din vector este: {0}", nrMic);

            // al doilea cel mai mare numar
            int m1 = 0;
            int m2 = 0;

            for (int i = 0; i < numere.Length; i++)
            {
                if (numere[i] > m1)
                {
                    m2 = m1;
                    m1 = numere[i];
                }
                else if (numere[i] > m2)
                {
                    m2 = numere[i];
                }
            }

            Console.WriteLine("Al doilea cel mai mare numar din vector este: {0}", m2); //298

            // numere lipsa
            int[] numereLipsa = new int[300 - numere.Length];
            int indexNrLipsa = 0;
            for (int i = 1; i <= 300; i++)
            {
                bool gasit = false;
                for (int j = 0; j < numere.Length; j++)
                {
                    if (i == numere[j])
                    {
                        gasit = true;
                        break;
                    }
                }

                if (gasit == false)
                {
                    numereLipsa[indexNrLipsa] = i;
                    indexNrLipsa++;
                }
            }

            Console.WriteLine("Lipsesc urmatoarele numere");
            for (int i = 0; i < numereLipsa.Length; i++)
            {
                Console.WriteLine(numereLipsa[i]);
            }

            // problema 2
            int[] v1 = {2, 4, 67, 1, 3, 56};
            int[] v2 = {4, 76, 34, 12, 1, 44, 3, 23};
            int gasite = 0;

            for (int i = 0; i < v1.Length; i++)
            {
                for (int j = 0; j < v2.Length; j++)
                {
                    if (v1[i] == v2[j])
                    {
                        gasite++;
                        Console.WriteLine("{0} se afla in ambii vectori.", v1[i]);
                    }
                }
            }

            Console.WriteLine("{0} elemente sunt comune in cei doi vectori.", gasite);

            Console.ReadKey();
        }
    }
}